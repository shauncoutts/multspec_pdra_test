Interview tasks.

You can choose ONE of two tasks
1) In the folder there is an image of a wheat field called ‘wheat6.jpeg’. Download this image to your machine. If you open it you should see a photo of a wheat field.
- a) Import this photo into a software platform of your choice (Python, R, Julia, etc.). Return the x and y coordinates for all the green pixels in this image.
- b) You will note that there are trees and other objects in the background. Exclude all the green pixels found in (a) that are NOT wheat.

2) There is a dataset in the folder called ‘NMDS_8D_prediction.csv’, download this to a local directory. This data has 25 predictors (denoted by X) and eight response variables (denoted by D). The response in each row is a coordinate position in an 8D space created using non-metric multi-dimensional scaling to reduce the dimentionality of a very high dimensional object. For each set of predictors (X’s in one row), we want to predict where in that 8D space a datum will sit (D’s in same row).
- a) What approach would you use for this task (Google is allowed). Why is this tool suited for this task? 
- b) Use a platform of your choice (Python, R, Julia, ect.) to show us how you would implement this approach (use the first 100 rows of data for the sake of speed).   